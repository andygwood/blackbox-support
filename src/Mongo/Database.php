<?php

namespace BlackBox\Support\Mongo;

use \BlackBox\Support\Contracts\Mongo\Database as DatabaseInterface;
use Config;
use Illuminate\Support\Facades\DB;
use League\Monga;

class Database implements DatabaseInterface{

    protected $db;

    public function __construct()
    {

        $config = Config::get('mongo');
        $username = array_get($config,'username');
        $password = array_get($config,'password');

        $user_connection_string = '';
        if(!empty($username)){
            $user_connection_string = $username.':'.$password.'@';
        }

        $connection_string = 'mongodb://'.$user_connection_string.array_get($config,'host').':'.array_get($config,'port');

        $client = new \MongoClient($connection_string);

        $db_name = array_get($config,'database');

        $env = env('APP_ENV');
        if($env === 'testing' || empty($env)){
            $db_name .= '_testing';
        }

        $db = $client->selectDB($db_name);

        $monga_connection = Monga::connection($client);
        $this->db = new Monga\Database($db,$monga_connection);

    }

    public function collection($collection)
    {
        return $this->db->collection($collection);
    }

}