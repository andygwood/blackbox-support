<?php

namespace BlackBox\Support\Mongo;

use BlackBox\Support\Contracts\Mongo\Database;
use BlackBox\Support\Data\Collection as ResponseCollection;
use BlackBox\Support\Data\Item;
use BlackBox\Support\Exceptions\HttpException;
use BlackBox\Support\Services\QueryFilter;
use Illuminate\Support\Facades\Log;
use League\Monga\Query\Find;

class Repository
{

	protected $collection;
	protected $collection_obj;
	protected $database;
	protected $timestamps = true;
	protected $created_at = 'created_at';
	protected $updated_at = 'updated_at';
	protected $fillable = [];

	public function __construct(Database $database)
	{

		$this->database = $database;

		if ($this->timestamps) {
			$this->fillable[] = $this->created_at;
			$this->fillable[] = $this->updated_at;
		}

		if (! is_null($this->collection)) {
			$this->collection($this->collection);
		}

	}

	public function collection($collection)
	{
		$this->collection_obj = $this->database->collection($collection);

		return $this;
	}

	protected function timestamps(array $input)
	{

		if ($this->timestamps) {
			$input[$this->created_at] = new \MongoDate();
			$input[$this->updated_at] = new \MongoDate();
		}

		return $input;
	}

	protected function makeIds($ids)
	{
		foreach ($ids as $key => $id) {
			$ids[$key] = $this->makeId($id);
		}

		return $ids;
	}

	protected function makeId($id)
	{

		if (! is_object($id) && \MongoId::isValid($id)) {
			return new \MongoId($id);
		}

		return $id;

	}

	/**
	 * @param $items
	 * @return ResponseCollection
	 */
	protected function format($items)
	{

		if (is_null($items)) {
			return new ResponseCollection();
		}

		return new ResponseCollection($items);
	}

	/**
	 * @param array $item
	 * @return Item
	 */
	protected function formatOne($item)
	{

		if (is_null($item)) {
			return new Item([]);
		}

		return new Item($item);
	}

	/**
	 * @return \League\Monga\Collection
	 */
	protected function getCollection()
	{
		return $this->collection_obj;
	}

	/*
	 * CREATE methods
	 */

	public function create(array $input)
	{

		if (isset($input['id'])) {
			unset($input['id']);
		}

		$input = $this->timestamps($input);

		$this->getCollection()->insert($input);

		return $this->formatOne($input);

	}

	public function createAll(array $input)
	{

		$items = [];
		foreach ($input as $i) {
			$items[] = $this->create($i);
		}

		return $this->format($items);

	}

	/*
	 * FIND methods
	 */
	public function take($limit, $offset = 0, array $where = [])
	{

		if (isset($where['id']) && ! is_null($where['id'])) {
			$where['_id'] = $this->makeId($where['id']);
			unset($where['id']);
		}

		return $this->format($this->getCollection()->find(function (Find $q) use (
			$where,
			$offset,
			$limit
		) {

			foreach ($where as $field => $value) {
				$q->where($field, $value);
			}

			$q->skip($offset);
			$q->limit($limit);

		})->toArray());
	}

	public function count($where = [])
	{
		return $this->getCollection()->find($where)->count();
	}

	public function find($id = null, array $where = [])
	{

		if (! is_null($id)) {
			$where['_id'] = $this->makeId($id);
		}

		return $this->format($this->getCollection()->find($where)->toArray());
	}

	public function findIn(array $ids)
	{

		$ids = $this->makeIds($ids);

		return $this->findQuery(function (Find $query) use ($ids) {

			$query->where('_id', ['$in' => $ids]);

		});
	}

	public function findQuery($query_closure)
	{
		return $this->format($this->getCollection()->find($query_closure)->toArray());
	}

	public function findOne(array $where = [])
	{
		return $this->formatOne($this->getCollection()->findOne($where));
	}

	protected function findOneQuery($query_closure)
	{
		return $this->formatOne($this->getCollection()->findOne($query_closure));
	}

	public function findOrFail($id_or_where)
	{

		if (is_array($id_or_where)) {
			$where = $id_or_where;
		} else {
			$where = [];
			$where['_id'] = $this->makeId($id_or_where);
		}

		$found = $this->getCollection()->findOne($where);

		if (is_null($found)) {

			if (is_array($id_or_where)) {
				$id_or_where = json_encode($id_or_where);
			}

			throw new \Exception('item could not be found: ' . get_called_class() . ' ' . $id_or_where);
		}

		return $this->formatOne($found);
	}

	public function findWithFilterQuery(QueryFilter $queryFilter)
	{

		return $this->findQuery(function (Find $q) use ($queryFilter) {

			$filters = $queryFilter->getFilterBy();
			foreach ($filters as $field => $value) {
				$q->where($field, $value);
			}

			$search = $queryFilter->getSearchBy();
			foreach ($search as $field => $value) {
				$q->whereLike($field, $value);
			}

			list($skip, $limit, $page) = $queryFilter->getPagination();
			$q->skip($skip)->limit($limit);

			list($field, $direction) = $queryFilter->getOrderBy();
			$q->orderBy($field, $direction);

		});

	}

	public function first(array $where = [])
	{

		return $this->findOne($where);

	}

	public function last($field = '_id', $direction = 'desc')
	{

		return $this->findOneQuery(function (Find $q) use ($field, $direction) {

			$q->orderBy($field, $direction);

		});

	}


	/*
	 * UPDATE methods
	 */

	public function update($id, array $input)
	{

		if (isset($input['id'])) {
			unset($input['id']);
		}

		$id = $this->makeId($id);

		if ($this->timestamps) {
			$input[$this->updated_at] = new \MongoDate();
		}

		$this->getCollection()->update(function (\League\Monga\Query\Update $query) use (
			$id,
			$input
		) {

			$query->where('_id', $id);

			foreach ($input as $key => $value) {
				if ($key == '_id') {
					continue;
				}
				$query->set($key, $value);
			}

		});

		$input['_id'] = $this->makeId($id);

		return $this->formatOne($input);

	}

	public function save(array $input)
	{

		if (isset($input['id']) && ! empty($input['id'])) {
			return $this->update($input['id'], $input);
		}

		return $this->create($input);

	}

	public function saveItem(Item $item)
	{
		return $this->save($item->toArray());
	}

	/*
	 * DELETE methods
	 */

	public function delete($id)
	{

		return $this->getCollection()->remove([
			'_id' => $this->makeId($id)
		]);

	}

	public function deleteOrFail($id)
	{

		$deleted = $this->delete($id);

		if (! $deleted) {
			throw new HttpException('the item could not be deleted: ' . get_called_class() . ' ' . $id);
		}

	}

	/**
	 *
	 * remove nested fields/documents
	 *
	 * @param $id
	 * @param $field
	 * @param $value
	 * @return bool
	 * @throws \Exception
	 * @throws \League\Monga\MongoCursorException
	 */
	public function pull($id, $field, $value)
	{

		return $this->getCollection()->update(function (\League\Monga\Query\Update $query) use (
			$id,
			$field,
			$value
		) {

			$query->whereId((string)$id);

			$query->pull($field, $value);

		});

	}

	public function truncate()
	{

		return $this->getCollection()->truncate();

	}

}