<?php namespace BlackBox\Support\Http\Controllers;

use BlackBox\Support\Exceptions\ValidationException;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Exception\HttpResponseException;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;
use League\Fractal\Resource\Item;
use App\Serializer\ArraySerializer;

abstract class Controller extends BaseController
{
	use DispatchesJobs, ValidatesRequests;

	protected $request_exclusions = ['tokenServerSession', 'headers', 'token'];
    protected $response;

    protected function getFractalManager()
    {

        $manager = new Manager();
        $manager->setSerializer(new ArraySerializer());

        $include = \Input::get('include');
        if(!empty($include)){
            $manager->parseIncludes($include);
        }

        return $manager;

    }

    public function collection($collection,$transformer)
    {

        $resource = new Collection($collection,$transformer);
        $output = $this->getFractalManager()->createData($resource)->toArray();
        return $output;

    }

    public function item($item,$transformer)
    {
        $resource = new Item($item,$transformer);
        $output = $this->getFractalManager()->createData($resource)->toArray();
        return $output;
    }

	protected function validateRequest(Request $request, array $rules = [])
	{
		return $this->validateInput($request->all(), $rules);
	}

	protected function validateInput(array $data = [], array $rules = [])
	{
		$v = Validator::make(
		  $data,
		  $rules
		);

		if ($v->fails()) {
			throw new ValidationException($v,522);
		}

		return $v;
	}

}
