<?php

namespace BlackBox\Support\Http\Middleware;

use BlackBox\Support\Services\Authentication;

class TokenAuth
{

    public function handle($request, \Closure $next)
    {

        $authentication = app(Authentication::class);
        $authentication->checkTokenAuth($request);

        return $next($request);
    }
}
