<?php

namespace BlackBox\Support\Http\Middleware;

use GuzzleHttp\Client;

class BlackBoxAuth
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, \Closure $next)
    {

        $client = new Client([
            'base_uri' => env('AUTH_API_BASE_URL').'v1/users/'
        ]);

        $token = $request->header('authorization',$request->get('bb_auth_token'));

        $client->request('GET','me',[
            'headers' => ['authorization' => $token]
        ]);

        return $next($request);
    }
}
