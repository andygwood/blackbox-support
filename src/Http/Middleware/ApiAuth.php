<?php

namespace BlackBox\Support\Http\Middleware;

use BlackBox\Support\Services\Authentication;

class ApiAuth
{

    public function handle($request, \Closure $next)
    {

        $authentication = app(Authentication::class);
        $authentication->checkApiAuth($request);

        return $next($request);
    }
}
