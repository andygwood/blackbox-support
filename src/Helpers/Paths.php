<?php

namespace BlackBox\Support\Helpers;

class Paths{

    public static function base_path($path='')
    {
        return dirname(dirname(dirname(__FILE__))).'/'.$path;
    }

}