<?php

namespace BlackBox\Support\Helpers;

class TokenUser{

    private static $instance;
    public $id;

    /**
     * @return TokenUser
     */
    public static function getInstance()
    {
        if (null === static::$instance) {
            static::$instance = new static();
        }

        return static::$instance;
    }

    public static function setId($id)
    {
        self::getInstance()->id = $id;
    }

    public static function id()
    {
        return self::getInstance()->id;
    }

}