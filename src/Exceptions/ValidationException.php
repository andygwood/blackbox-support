<?php

namespace BlackBox\Support\Exceptions;

use Illuminate\Contracts\Validation\Validator;

class ValidationException extends BaseException
{

    public function __construct(Validator $v, $code = 400)
    {
        $messages = $v->getMessageBag()->getMessages();
        parent::__construct($messages, $code);
    }

}