<?php

namespace BlackBox\Support\Exceptions;

use Exception;

class BaseException extends Exception
{
    /**
     * Handle base exceptions.
     *
     * @param null $message
     * @param int $code
     * @param Exception $previous
     */
    public function __construct($message = null, $code = 500, Exception $previous = null)
    {
        $message = json_encode($message);

        parent::__construct($message, $code, $previous);

    }

}
