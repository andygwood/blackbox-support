<?php
namespace BlackBox\Support\Exceptions;

class HttpException extends BaseException
{

    public function __construct($message, $code = 400)
    {
        parent::__construct($message, $code);
    }

}