<?php namespace BlackBox\Support\Providers;

use BlackBox\Support\Helpers\Paths;
use Illuminate\Support\ServiceProvider;

class SupportServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            Paths::base_path('config/allowed_mime_types.php') => config_path('allowed_mime_types.php'),
            Paths::base_path('config/mime_types.php') => config_path('mime_types.php'),
            Paths::base_path('config/jwt.php') => config_path('jwt.php'),
            Paths::base_path('config/mongo.php') => config_path('mongo.php'),
        ], 'config');
    }

    /**
     * Register any application services.
     *
     * This service provider is a great spot to register your various container
     * bindings with the application. As you can see, we are registering our
     * "Registrar" implementation here. You can add your own bindings too!
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(
            'BlackBox\Support\Contracts\Mongo\Database',
            'BlackBox\Support\Mongo\Database'
        );
    }
}
