<?php

namespace BlackBox\Support\Contracts\Mongo;

interface Database{

    public function collection($collection);

}