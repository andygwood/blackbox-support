<?php

class MongoTest extends TestCase
{
    
    /**
     * @return \BlackBox\Support\Mongo\Repository
     */
    protected function getRepo()
    {
        
        $repo = app('\BlackBox\Support\Mongo\Repository');
        $repo->collection('yolo');
        return $repo;
        
    }
    
    /**
     * @return \BlackBox\Support\Services\QueryFilter
     */
    protected function getQueryFilter()
    {
        
        return app('\BlackBox\Support\Services\QueryFilter');
        
    }
    
    public function testCreate()
    {
        
        $repo = $this->getRepo();
        
        $item = $repo->create([
            'hello' => 'world'
        ]);
        
        $this->assertInstanceOf('MongoId',$item->get('_id'));
        
    }
    
    public function testCount()
    {
        $repo = $this->getRepo();
        $repo->save(['test' => 'data']);
        $this->assertGreaterThan(0,$repo->count());
    }
    
    public function testFind()
    {
        
        $repo = $this->getRepo();
        
        $this->assertInstanceOf('\BlackBox\Support\Data\Collection',$repo->find());
        
    }
    
    public function testFindOne()
    {
        
        $repo = $this->getRepo();
        
        $this->assertInstanceOf('\BlackBox\Support\Data\Item',$repo->findOne());
        
    }
    
    public function testUpdate()
    {
        
        $repo = $this->getRepo();
        
        $item = $repo->create([
            'hello' => 'world'
        ]);
        
        $updated = $repo->update($item->get('id'),['hello' => 'worldz']);

        $this->assertEquals('worldz',$updated->hello);
        
        
    }
    
    public function testSave()
    {
        
        $repo = $this->getRepo();
        
        $saved = $repo->save(['hello' => '123']);
        
        $this->assertInstanceOf('\BlackBox\Support\Data\Item',$saved);
        
    }
    
    public function testDelete()
    {
        
        $repo = $this->getRepo();
        
        $item = $repo->create([
            'hello' => 'worldzz'
        ]);
        
        $deleted = $repo->delete($item->get('id'));
        
        $this->assertEquals(true,$deleted);
        
    }

    public function testDeleteNested()
    {

        $repo = $this->getRepo();

        $item = $repo->create([
            'hello' => [
                'world' => '12345'
            ]
        ]);

        $deleted = $repo->pull($item->id,'hello',['world' => '12345']);

        $this->assertEquals(true,$deleted);

    }
    
    public function testFilter()
    {
        $repo = $this->getRepo();
        
        $repo->create([
            'field' => 'value'
        ]);
        
        $qf = $this->getQueryFilter();
        $qf->filterBy(['field' => 'value']);
        $items = $this->getRepo()->findWithFilterQuery($qf);
        
        $this->assertGreaterThan(0,$items->count());
        
        
    }
    
    public function testSearch()
    {
        $repo = $this->getRepo();
        
        $repo->create([
            'some_search_field' => 'yabadabadooooo'
        ]);
        
        $qf = $this->getQueryFilter();
        $qf->search(['some_search_field' => '%abada%']);
        $items = $this->getRepo()->findWithFilterQuery($qf);
        
        $this->assertGreaterThan(0,$items->count());
        
        
    }
    
    public function testPagination()
    {
        $repo = $this->getRepo();
        
        $repo->create([
            'something' => 'nadda'
        ]);
        
        $qf = $this->getQueryFilter();
        $qf->paginate(1,1);
        $items = $this->getRepo()->findWithFilterQuery($qf);
        
        $this->assertEquals(1,$items->count());
        
    }
    
    public function testOrderBy()
    {
        $repo = $this->getRepo();
        
        $repo->create([
            'first' => 'item'
        ]);
        
        sleep(0.5);
        
        $repo->create([
            'second' => 'item'
        ]);
        
        $qf = $this->getQueryFilter();
        $qf->orderBy('created_at','desc');
        $items = $this->getRepo()->findWithFilterQuery($qf);
        
        $first = $items->first();
        $last = $items->last();
        $this->assertGreaterThan($last->created_at,$first->created_at);
        
    }
    
}
