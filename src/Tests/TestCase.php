<?php

namespace BlackBox\Support\Tests;

use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Facades\JWTFactory;

class TestCase extends \TestCase{

    protected function getToken()
    {

        $custom_claims = ['testing' => true];

        //sub is a user id
        $payload = JWTFactory::sub('andrew.wood@70ms.com')->custom($custom_claims)->make();

        $token = JWTAuth::encode($payload);

        return (string)$token;
    }

    protected function callWithApiAuth($method, $uri, $data = [], $headers = [])
    {
        $headers['HTTP_CLIENT_ID'] = env('HTTP_CLIENT_ID');
        $headers['HTTP_CLIENT_SECRET'] = env('HTTP_CLIENT_SECRET');

        if ($method === 'get') {
            return $this->get($uri, $headers);
        }

        return $this->$method($uri, $data, $headers);

    }

    protected function callWithAuth($method, $uri, $data = [], $headers = [])
    {
        $headers['HTTP_AUTHORIZATION'] = $this->getToken();

        if ($method === 'get') {
            return $this->get($uri, $headers);
        }

        return $this->$method($uri, $data, $headers);
    }

}