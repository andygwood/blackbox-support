<?php

namespace BlackBox\Support\Data;

use Illuminate\Http\Request;

class Item{

    protected $item;

    public function __construct(array $item){

        if(!isset($item['id']) && isset($item['_id'])){
            $id = $item['_id'];
            unset($item['_id']);
            $item['id'] = (string)$id;
        }

        $this->item = $item;
    }

    public function __get($key)
    {

        return $this->get($key);
    }

    public function __set($key,$value)
    {
        $this->set($key,$value);
    }

    public function get($key,$default=null)
    {
        return array_get($this->item,$key,$default);
    }

    public function set($key,$value)
    {
        $this->item[$key] = $value;
    }

    public function setFromRequest(Request $request)
    {
        $input = $request->all();
        foreach($input as $key => $value){
            $this->set($key,$value);
        }
    }

    public function toArray()
    {
        $item = $this->item;
        return $item;
    }

    public function only(array $keys)
    {
        $items = [];
        foreach($keys as $key => $default){
            if(is_numeric($key)){
                $items[$default] = $this->get($default);
            }else{
                $items[$key] = $this->get($key,$default);
            }
        }
        return $items;
    }

}