<?php

namespace BlackBox\Support\Data;

use Illuminate\Support\Collection as LaravelCollection;

class Collection extends LaravelCollection{

    public function __construct(array $items = []){

        foreach($items as $key => $item){

            if(!is_array($item)){
                continue;
            }

            $items[$key] = new Item($item);
        }

        parent::__construct($items);

    }

    public function toArray()
    {

        $output = [];
        foreach($this->items as $item){
            $output[] = $item->toArray();
        }

        return $output;

    }

}