<?php

namespace BlackBox\Support\Services;

class QueryFilter{

    protected $filter_by;
    protected $search_by;
    protected $page;
    protected $per_page;
    protected $order_by_field;
    protected $order_direction;

    public function __construct()
    {
        $this->reset();
    }

    public function reset()
    {
        $this->filter_by = [];
        $this->search_by = [];
        $this->page = 1;
        $this->per_page = 50;
        $this->order_by_field = '_id';
        $this->order_direction = 'asc';
    }

    public function getFilterBy()
    {
        return $this->filter_by;
    }

    public function getSearchBy()
    {
        return $this->search_by;
    }

    public function getPagination()
    {
        return [($this->page-1)*$this->per_page,$this->per_page,$this->page];
    }

    public function getOrderBy()
    {
        return [$this->order_by_field,$this->order_direction];
    }

    public function filterBy(array $where)
    {

        $this->filter_by = $where;

    }

    public function search(array $where_like)
    {

        $this->search_by = $where_like;

    }

    public function paginate($page_number,$per_page=0)
    {

        $page_number = (int)$page_number;
        $per_page = (int)$per_page;

        if($per_page < 1){
            $per_page = 1;
        }

        if ($page_number < 1){
            $page_number = 1;
        }

        $this->per_page = $per_page;
        $this->page = $page_number;

    }

    public function orderBy($order_by_field,$order_by_direction='asc')
    {
        $this->order_by_field = $order_by_field;
        $this->order_direction = $order_by_direction;
    }

}