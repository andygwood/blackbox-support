<?php

namespace BlackBox\Support\Services\MediaManipulations;

use Intervention\Image\Image;
use Intervention\Image\Facades\Image as ImageFacade;

class Fit
{

    public function make($resource,$options)
    {

        $image = ImageFacade::make($resource);

        $width = array_get($options,'width');
        $height = array_get($options,'height');

        $image->fit($width,$height, function ($constraint) {
            $constraint->upsize();
        });

        $stream = $image->stream();
        $image->destroy();

        return [
            'stream' => $stream,
            'prefix' => 'fit-'.$width.'x'.$height
        ];

    }

}