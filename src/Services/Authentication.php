<?php

namespace BlackBox\Support\Services;

use BlackBox\Support\Exceptions\HttpException;
use BlackBox\Support\Mongo\Repository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Modules\V1\Events\TokenAuthSuccess;
use Tymon\JWTAuth\Facades\JWTAuth;
use Tymon\JWTAuth\Facades\JWTFactory;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;

class Authentication{

    public function generateClientId()
    {
        return bin2hex(openssl_random_pseudo_bytes(32));
    }

    public function generateClientSecret($client_id)
    {
        $custom_claims = ['type' => 'api'];
        $payload = JWTFactory::sub($client_id)->custom($custom_claims)->make();
        return (string)JWTAuth::encode($payload);
    }

    public function login(Repository $repository,array $credentials)
    {

        if(!isset($credentials['password'])){
            throw new \Exception('login credentials need to include a password');
        }

        $password = $credentials['password'];
        unset($credentials['password']);

        $item = $repository->findOne($credentials);

        if($item->suspended){
            throw new HttpException('the user is suspended');
        }

        if(!Hash::check($password,$item->password)){
            throw new HttpException('email or password is incorrect');
        }

        $custom_claims = array_merge($item->toArray(),['type' => 'auth']);
        $payload = JWTFactory::sub($item->id)->custom($custom_claims)->make();

        $item->token = (string)JWTAuth::encode($payload);
        return $item;

    }

    public function logout($token)
    {
        JWTAuth::invalidate($token);
    }

    public function checkApiAuth(Request $request)
    {
        if (! $token = $request->header('client_secret',$request->get('client_secret'))) {
            throw new HttpException('api token not provided',400);
        }

        try {

            $payload = JWTAuth::getPayload($token);

            if($payload->get('sub') !== $request->header('client_id',$request->get('client_id'))){
                throw new HttpException('invalid subject');
            }

            if($payload->get('custom.type') !== 'api'){
                throw new HttpException('invalid type');
            }

        } catch (TokenExpiredException $e) {
            throw new HttpException('token expired',$e->getStatusCode());
        } catch (JWTException $e) {
            throw new HttpException('token invalid',$e->getStatusCode());
        }
    }

    public function checkTokenAuth(Request $request)
    {
        if (! $token = $request->header('authorization',$request->get('bb_auth_token'))) {
            throw new HttpException('auth token not provided',400);
        }

        try {

            $payload = JWTAuth::getPayload($token);
            event(new TokenAuthSuccess($payload->get('sub')));

        } catch (TokenExpiredException $e) {
            throw new HttpException('token expired',$e->getStatusCode());
        } catch (JWTException $e) {
            throw new HttpException('token invalid',$e->getStatusCode());
        }
    }

}