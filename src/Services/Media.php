<?php

namespace BlackBox\Support\Services;

use BlackBox\Support\Exceptions\HttpException;
use BlackBox\Support\Services\MediaManipulations\Fit;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Storage;
use Intervention\Image\Facades\Image;

class Media
{

    public function isImage($mime_type)
    {

        $mime_types = Config::get('allowed_mime_types.images');

        if (in_array($mime_type, $mime_types)) {
            return true;
        }

        return false;

    }

    public function upload($files, $options = [])
    {

        $items = [];
        foreach ($files as $file) {

            if (!$file->isValid()) {
                continue;
            }

            $filename = uniqid() . '.' . $file->extension();
            $filepath = 'media/';
            $full_filepath = $filepath . $filename;

            $tmp_filepath = $file->path();
            //$tmp_stream = fopen($tmp_filepath, 'rw');
            Storage::put($full_filepath, file_get_contents($tmp_filepath));

            $mime_type = $file->getMimeType();

            $items[$filename] = $item = [
                'original' => $file->getClientOriginalName(),
                'filepath' => $filepath,
                'filename' => $filename,
                'mime_type' => $mime_type,
                'extension' => $file->extension(),
                'filesize' => Storage::size($full_filepath)
            ];

            $items[$filename] = $this->manipulate($item, $options);

        }

        return $items;

    }

    public function manipulate(array $item, array $options)
    {

        if (!$this->isImage($item['mime_type'])) {
            return $item;
        }

        $full_filepath = $item['filepath'].$item['filename'];

        $manipulations = array_get($options, 'manipulations', []);
        if (!is_array($manipulations)) {
            return $item;
        }

        $types = [
            'fit' => new Fit()
        ];

        foreach ($manipulations as $manipulation) {

            if (!is_array($manipulation)) {
                continue;
            }

            $type = array_get($manipulation, 'type');

            if (!array_key_exists($type, $types)) {
                throw new HttpException('you need to supply a manipulation type: ' . json_encode($types));
            }

            $stream = Storage::getDriver()->readStream($full_filepath);
            $output = $types[$type]->make($stream,$manipulation);

            if(isset($item[$output['prefix']])){
                continue;
            }

            $output_stream = $output['stream'];

            $new_item = $item;
            $new_item['prefix'] = $output['prefix'];
            $new_item['filename'] = $output['prefix'].'-'.$item['filename'];
            $new_item_full_filepath = $new_item['filepath'].$new_item['filename'];

            if(isset($new_item['manipulations'])){
                unset($new_item['manipulations']);
            }

            Storage::put($new_item_full_filepath,$output_stream);

            $new_item['filesize'] = Storage::size($new_item_full_filepath);

            $item_manipulations = array_get($item,'manipulations',[]);

            $item_manipulations[] = $new_item;

            $item['manipulations'] = $item_manipulations;
            $item[$output['prefix']] = $new_item['filename'];

        }

        return $item;

    }

}