<?php

return [
    \Tymon\JWTAuth\Providers\JWTAuthServiceProvider::class,
    \BlackBox\Support\Providers\SupportServiceProvider::class,
    'Sorskod\Larasponse\LarasponseServiceProvider',
    'Intervention\Image\ImageServiceProvider'
];