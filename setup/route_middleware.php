<?php

return [
    'token.auth' => BlackBox\Support\Http\Middleware\TokenAuth::class,
    'api.auth' => BlackBox\Support\Http\Middleware\ApiAuth::class,
    'blackbox.auth' => BlackBox\Support\Http\Middleware\BlackBoxAuth::class
];