<?php
return array(
  'videos' => array(
    'video/mp4',
    'video/quicktime',
    'video/vnd.rn-realvideo',
    'video/mpeg',
    'video/x-msvideo',
    'video/msvideo',
    'video/avi',
    'application/x-troff-msvideo',
    'video/x-sgi-movie',
    'video/3gpp2',
    'video/3gp',
    'video/mp4',
    'video/mp4',
    'video/webm',
    'application/videolan',
    'video/x-ms-wmv',
    'video/x-ms-asf',
    'video/x-ms-asf'
  ),
  'images' => array(
    'image/jpeg',
    'image/pjpeg',
    'image/png',
    'image/x-png',
    'image/gif'
  ),
);