<?php

return [
    'host'     => 'localhost',
    'port'     => 27017,
    'username' => '',
    'password' => '',
    'database' => env('DB_DATABASE', 'BlackBox')
];