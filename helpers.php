<?php

function media_url($filename)
{
    return env('MEDIA_URL').$filename;
}

function r_rmdir($path)
{
    $files = glob($path . '/*');
    foreach ($files as $file) {
        is_dir($file) ? r_rmdir($file) : unlink($file);
    }
    rmdir($path);
    return;
}

function ext_from_mime($mime_type)
{

    $mime_types = Config::get('mime_types');

    foreach ($mime_types as $ext => $config_mime_type) {

        if (is_array($config_mime_type) && in_array($mime_type, $config_mime_type)) {
            return $ext;
        }

        if ($config_mime_type === $mime_type) {
            return $ext;
        }

    }

}

function item_time_ago($time)
{

    $minutes = round(abs(time() - strtotime($time)) / 60, 2);

    if ($minutes < 1) {
        return 'less than a minute ago.';
    }

    if ($minutes > 59) {
        $hours = round(abs($minutes) / 60, 2);

        if ($hours < 2) {
            return 'about an hour ago.';
        }

        if ($hours > 23) {
            return 'more than a day ago.';
        }

        if ($hours > 9) {
            return '+9 hours ago.';
        }

        return round($hours, 0) . ' hours ago.';
    }

    return round($minutes, 0) . ' minutes ago.';

}

function human_date($timestamp)
{

    if ($timestamp instanceof MongoDate) {
        $timestamp = $timestamp->sec;
    }

    return date('D F jS @ g:ia', $timestamp);

}